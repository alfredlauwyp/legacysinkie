---
layout: ../layouts/AboutLayout.astro
title: "About"
---
# About Myself
Insert Image here
Hey I'm Alfred Lauw, L2 Judge from Singapore. I started playing Magic the Gathering when I was getting my degree in Australia and chanced upon the Mono Blue Standard deck that was popular at that time  INSERT TEMPEST DJINN HERE and ordered the whole deck from mtgmint to try and play it. I had a friend who was playing Mono Green and we had fun playing that matchup. Little did I not know that standard was about to be rotated. I realised commander was a new fun thing and gave that a go. I thought it was really exciting because of the wide pool of cards to choose from. I eventually realised I don't like the casual aspect about it and that it resulted in draggy games and games where my losses would be out of my control more often than not.

When Ultimate Masters was released, I was back in Singapore for holiday and I was interested in trying Modern. I was convinced by some other players that I should get the reprinted Cavern of Souls so that I can play Elves which I was very interested in. I figured out my color pairing rather early into playing and was mostly a Green-White boy. I played a bunch and got destroyed by Phoenix at the time due to The Thing in the Ice being super powerful against creature based strategies. It was at this time I decided my goal was to get better at the game and not just "play for the sake of playing".

After graduating and coming back to Singapore, I got really sick of Modern due to the abrupt change in meta from Modern Horizons and Oko. So I decided to dive into Legacy. I always joked with Kenji that I would go and join Friday Legacy with Bogles without duals and see how that will fare (I have not attempted it till this day). I ended up buying into Bant Food Chain which was close to my type of playstyle which was combo-midrange. Kenji followed along by first making Burn and then moving into Eldrazi Stompy. COVID came at a relatively nice time because shops lost foot traffic but they were trying to engage sales via online. We decided to go all in on Legacy because we were hooked. We collectively bought a bunch of dual lands that enabled us to play most decks. I had my Force of Wills while Kenji was patiently waiting for a reprint which also did come true.

We dedicated time to play against each other on various matchup, hoping to help each other get better. I think this was an invaluable experience we had. From there, the group expanded as we managed to convince another friend of ours to buy City of Traitors as he was a good ol' stompy boi still crying from the demise of Simian Spirit Guide in Modern. We made friends amongst the Legacy community and here we are. Kenji was applying to be a judge and he asked me if I wanted to join in, and I agreed and we have both attained L2.




## Legacy in Singapore
Legacy is currently the format having the highest turnout of about 20+ people on the weeknights. Every night has an event firing somewhere.

### Monday
[Dueller's Point](https://www.duellerspoint.com/) 20:00HRS

### Tuesday
[Mox&Lotus](https://moxandlotus.sg/) 20:00HRS


### Wednesday
[Grey Ogre Games](https://greyogregames.com) 19:30HRS\
$10 Entry
3 Wins - 15 Store Credit + Foil Promo Pack
2 Wins - 10 Store Credit + Non-Foil Promo Pack
0/1 Win - 5 Store Credit

## Thursday
[Hideout](https://www.hideout-online.com/) 20:00HRS\
$8 Entry
3 Wins - 12 Store Credit + Promo Pack
2 Wins - 8 Store Credit
0/1 Win - 4 Store Credit

## Friday
[Games Haven Paya Lebar](https://gameshaventcg.com) 20:00HRS
$8 Entry
3 wins - 12 Store Credit + Promo Pack
2 Wins - 8 Store Credit
0/1 Win - 4 Store Credit

<!-- <div>
  <img src="/assets/dev.svg" class="sm:w-1/2 mx-auto" alt="coding dev illustration">
</div> -->

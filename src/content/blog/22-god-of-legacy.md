---
title: God of Legacy
author: Alfred Lauw
pubDatetime: 2023-04-24T00:00:00Z
postSlug: godoflegacy-22
featured: true
draft: false
tags:
  - legacy
ogImage: ""
description: The 22nd God of Legacy Tournament
---
## The 22nd God of Legacy Tournament
9 Rounds Swiss 239 Players T_T\
Final Result: 41st Place with score 5-3-1\
Deck: [4C Cascade](https://moxfield.com/deck/... "4C Cascade")

TO INSERT DECK IMAGE AND FIX DECKLIST LINK

### TLDR:
- Big thank you to Ark4n for innovating on this deck.
- Started on the draw every round! Die roll was not in my favor
- I strongly believe in the power of the deck through my time playing with it in Singapore and putting it through this tournament.
- Minsc & Boo carries the deck hard later in the turns. YABAI!
- Leyline Binding is amazing but the mana is tight and we can't add an untapped source (triome) to add black.
- Japanese players are very polite, they play well and are not salty about their losses.
- Am very motivated to learn Japanese more so that I'm better equipped for travel and Eternal Weekend.
- Might cut the 4th Brazen Borrower or Leyline Binding for 1 more Fire//Ice, Fury in the sideboard overperformed against Tribal and Control.

### R1 Jeskai Saga WW-

G1 Fire//Ice came in handy here on the draw, tapping down his Urza's Saga before he can make a karnstruct to gum the board. 1 shardless agent followed by a violent outburst seals the deal while he was digging real hard for supreme verdict.

G2 T3 Minsc&Boo eats his Force of Will, which lets me cast my second copy. Then followed up by Shardless Agent.

### R2 RW Initative WLL
G1 Managed to sneak the win by my opponent not knowing what I'm playing and going ahead with Chalice on 1 (which in a blind is still the absolute correct option).

G2 T1 Uncounterable 3 mana hatebears win the game

G3 T1 Uncounterable 3 mana hatebears win the game again...

### R3 UG Omnitell WW-
G1 Pretty uninteresting G1 as we have not much action for the first 2 turns and I was just making land drops. He fetched Snow Forest and Snow Island which put him on either Aluren or Omnitell and the good takeaway from G1 was that he used Force pitching Cunning Wish which allowed me to determine he was on Omnitell. Got the kill with Shardless Agent + Violent Outburst.

G2 Opponent keeps a hand banking on EE on 0 and I do the usual pumpfake to convince them they are safe. Opponent unfortunately brainstorm locks themself on T2 and plays his Boseiju, Who Endures as 2nd land. This allowed me to setup a hand with Force + Mystical Dispute for 2 Show and Tells. I play Minsc & Boo, and opponent concedes after. 

### R4 Yorion D&T WW-
This round was a nail-biter.

G1 Being on the draw in this matchup is not great even though I think this matchup is meant to be good for us. T1 Mother of Runes is bad and it did happen. Thankfully there was no T2 Thalia which allowed me to play out my Shardless Agent. The first 2 Rhinos eat a Swords to Plowshares and a Solitude but the second shardless agent with Minsc & Boo closed the game. Got wastelanded once and it was okay. The deck is pretty resilient even if its mana is attacked (in my opinion).

G2 Opponent has T1 Mother of Runes and seeing my hand was ESG,ESG,SSG, Fury, Brotherhood's End, Island, Scalding Tarn, I couldn't risk Mother protecting his hatebear like Thalia or Aethersworn Canonist or Peacekeeper. So I make the tough choice to trade 3 cards for the Mother of Runes via Bro's End, keeping the Fury for a better future use (to potentially 2 for 3-4). I continue to curve out and eventually cast a Shardless Agent to gain some board presence which was greeted by 2 back to back Flickerwisps. Thankfully I continued to make land drops and hard cast Fury against the board and 1 for 3 them. Solitude answers my Fury but I continue to not progress my board for a turn or two, meanwhile opponent continues to grow his board wider with Mother of Runes and other creatures. I resort to casting ESG and SSG to put more bodies. Life totals on the last turn was ME 1 : OPP 4. Opponent was attempting to race me and only had 2 blockers left behind. I attack with ESG and SSG pressuring lethal, Opponent chumps one with mother and doesn't block the other (which is safe around violent outburst). I cast Minsc & Boo which is followed up with "Yabai". I minus Minsc for the kill.

Opponent played a really good game here.

### R5 Jeskai Control LW-
This round was a slogfest. We ended up being the last table and I believe I had all my lands in play in G3.

G1 I got caught offguard by a mainboard flusterstorm. Opponent got Narset into Teferi into instant speed Day's Undoing. I concede so that I still have a chance to win G2 and G3 for time. 

G2 Opponent brings in Meddling Mage and ironically names Uro with it eventually instead of Crashing Footfalls (which I think is the correct play) as I was poised to play the longer game. I Fire 1-1 to Narset and Teferi which unlocks my Cascade. Then I proceed to hardcast Fury the board and beat for lethal.

G3 I think I made a bad play this game by fighting over Prismatic Ending on my 7/7 Boo which allowed for opponent to cleanly answer my Minsc with Hydroblast (I deeply regret this play as I think it cost me the win).
Opponent answers all 6 of my 4/4s created, Meddling Mage names Fury. We reach a point where my life total is 30++ and he is at 4. But control deck delivers and puts out all 3 Walkers but we have gone to time in the round and opponent does not have enough time to get to JTMS Ultimate me. We Draw and I was kind of happy with the outcome because I know where my mistake was made and I was planning to 7-1-1 to make it to top8.

### R6 Elves LWW
G1 I struggle to build a board while he continues to draw cards. Opponent makes a sweet play here as I Force his Glimpse and he responds with Boseiju on my Leyline Binding holding his Allosaurus Shepherd hostage. This leads to a blow-out and he proceeds to beat for lethal.

G2 T2 Fire his Dryad Arbor + Allosaurus. T4 Brotherhood's End wipes the board again and I proceed to beat for lethal with a pair of 4/4s and Minsc & Boo.

G3 Being on the play is pretty rought cause it means my T2 Fire or Bro's End might actually be really bad assuming theres a Wirewood or Quirion on the field. Thankfully none were there and I manage to wipe the board once. Opponent builds back a little and glimpses, finding Grist, he makes a really hard decision here of whether to minus to deal with a 4/4 or greedier plus to grow his board. He goes with the conservative minus on my 4/4. I proceeded to cast Minsc & Boo, flinging the Boo at his last 1/1 Elf on board since he will have no way to remove the M&B after. Blessed by a topdeck Violent Outburst, I beat for lethal with a rhino + 4/4 Boo with +1 power.

Very happy with my sideboard choice of Fury this tournament as it paid off real hard in this matchup.
Every single time I've cast Minsc&Boo, opponent always says Yabai!!

### R7 UB Tezzerator LL-
Oh man this deck really impressed me! I thought it would be a nice matchup like 8Cast but it definitely wasn't.

G1 I successfully T2 Shardless Agent on the draw and beat in once for 10 life. But after that opponent goes on a rampage and makes grows his karnstruct to 9/9 and puts Skorpekh Lord on the BF making his karnstructs 10/9 menace. That was unbeatable and I conceded.

G2 Opponent casts Chalice on 0. I attempt to be the better "hate bear" and cast Shardless Agent which is met with huge confusion until I flip to Stony Silence. Opponent realises what's up and commands a force which my hand did not have a way to deal with regretfully. 4 Strixes are cast on his side (amazing) and I proceed to lose from there as he continues to gain card advantage.

### R8 RB Reanimator WW-
G1 Opponent mulls to 5 which I guess was a combo deck. T1 Marsh Flats, fetch badlands into faithless looting tells it all and I heave a very big sigh of relief as its Reanimator. I cast a T3 Minsc & Boo with Force backup for any of his reanimation spell but he didn't get there. Opponent concedes.

G2 My starting hand has Shardless Agent, Endurance, Force, SSG, Plateau, Fetchland, Leyline Binding
Opponent goes T1 Faithless Looting dropping an Atraxa into the GY. My starting hand has T2 Shardless Agent to get Containment Priest but I funnily draw the Containment Priest (HAS HAPPENED TOO MANY TIMES). T2 Opponent
casts Dark Ritual into Renimate. I flash Containment Priest which is followed up with a "Yabai" and opponent casts Shallow Grave which eats my Force pitch Shardless. Opponent sighs and loses 7 life. Containment Priest beats for 3 turns, opponent makes last ditch effort and casts Grief which reveals Endurance, Brazen, Leyline Binding, and he proceeds to concede.

I love this matchup so much!

### R9 UR Delver LWL
I got paired up 18pt vs 21pt but neither of us were in Top8 contention and the round was played out.

G1 On the draw against Delver is always rough since we gotta fight through both Daze and Force while trying to be fast. Opponent was playing with Bloodbraid Marauder which gave him some extra bodies from cascading into Delver.

G2 On the play I setup Shardless with the ability to pay for Daze and deal with force with Dispute. Quick beatdown.

G3 Walked into a Daze + Force and opponent played threat after threat. I was able to stem that by playing Petty Theft on the 5/5 Murktide but it only slowed the death.

Need to play this matchup more!